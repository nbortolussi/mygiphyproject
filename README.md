# My Giphy Project

This project is an infinite scroll GIF crawler. View trending GIFs or search for new ones. Click on any GIF to start animation. Click Full Screen button to view the GIF in full quality.

## API Usage

	- Used Giphy trending API for initial home screen.
	- Used Giphy search API for search calls.
	
## Third Party SDKs Used

	- Used several third party SDKs that I like to use in my own projects.
	- Retrofit for making REST calls.
	- OKHTTP as an HTTP client.
	- MaterialProgressBar for beautiful loading.
	- Fresco for image loading / caching.