package com.mygiphyproject;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;

/**
 * Created by nbortolussi on 5/25/16.
 */
public class FrescoHelper {
    private static DraweeController draweeController;

    public static DraweeController getDraweeController() {
        if (draweeController == null)
            draweeController = Fresco.newDraweeControllerBuilder()
                    .setAutoPlayAnimations(true)
                    .build();

        return draweeController;
    }
}
