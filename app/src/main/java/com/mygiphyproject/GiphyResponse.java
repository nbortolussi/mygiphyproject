package com.mygiphyproject;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by nbortolussi on 5/25/16.
 */
public class GiphyResponse {
    private ArrayList<GIF> data;
    private Pagination pagination;

    public ArrayList<GIF> getData() {
        return data;
    }

    public void setData(ArrayList<GIF> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public static class GIF implements Parcelable {
        private Images images;

        public Uri getImageUri() {
            return Uri.parse(images.fixed_width_downsampled.url);
        }

        public Uri getFullSizeImageUri() {
            return Uri.parse(images.original.url);
        }

        public int getHeight(double width) {
            double originalHeight = images.fixed_width_downsampled.height;
            double originalWidth = images.fixed_width_downsampled.width;
            return (int) ((originalHeight / originalWidth) * width);
        }

        public int getFullSizeHeight(double width) {
            double originalHeight = images.original.height;
            double originalWidth = images.original.width;
            return (int) ((originalHeight / originalWidth) * width);
        }

        public static class Images implements Parcelable {

            private Image fixed_width_downsampled;
            private Image original;

            public static class Image implements Parcelable {
                private String url;
                private double width;
                private double height;

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.url);
                    dest.writeDouble(this.width);
                    dest.writeDouble(this.height);
                }

                protected Image(Parcel in) {
                    this.url = in.readString();
                    this.width = in.readDouble();
                    this.height = in.readDouble();
                }

                public static final Creator<Image> CREATOR = new Creator<Image>() {
                    @Override
                    public Image createFromParcel(Parcel source) {
                        return new Image(source);
                    }

                    @Override
                    public Image[] newArray(int size) {
                        return new Image[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeParcelable(this.fixed_width_downsampled, flags);
                dest.writeParcelable(this.original, flags);
            }

            protected Images(Parcel in) {
                this.fixed_width_downsampled = in.readParcelable(Image.class.getClassLoader());
                this.original = in.readParcelable(Image.class.getClassLoader());
            }

            public static final Creator<Images> CREATOR = new Creator<Images>() {
                @Override
                public Images createFromParcel(Parcel source) {
                    return new Images(source);
                }

                @Override
                public Images[] newArray(int size) {
                    return new Images[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.images, flags);
        }

        protected GIF(Parcel in) {
            this.images = in.readParcelable(Images.class.getClassLoader());
        }

        public static final Parcelable.Creator<GIF> CREATOR = new Parcelable.Creator<GIF>() {
            @Override
            public GIF createFromParcel(Parcel source) {
                return new GIF(source);
            }

            @Override
            public GIF[] newArray(int size) {
                return new GIF[size];
            }
        };
    }

    public static class Pagination {
        private int count;
        private int offset;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }
    }
}
