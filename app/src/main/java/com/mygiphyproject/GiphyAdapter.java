package com.mygiphyproject;

import android.graphics.drawable.Animatable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;


public class GiphyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private ArrayList<GiphyResponse.GIF> data;
    private int rowWidth;

    public GiphyAdapter(int rowWidth) {
        this.data = new ArrayList<>();
        this.rowWidth = rowWidth;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GifViewHolder gifViewHolder = (GifViewHolder) viewHolder;
        GiphyResponse.GIF GIF = data.get(position);

        gifViewHolder.gif.setImageURI(GIF.getImageUri());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = GIF.getHeight(rowWidth);
        gifViewHolder.gif.setLayoutParams(params);

        gifViewHolder.gif.setOnClickListener(this);
        gifViewHolder.viewFullSizeGIF.setTag(GIF);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_gif, viewGroup, false);
        return new GifViewHolder(itemView);
    }

    @Override
    public void onClick(View v) {
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) v;
        Animatable animatable = simpleDraweeView.getController().getAnimatable();
        if (animatable != null) {
            if (animatable.isRunning())
                animatable.stop();
            else
                animatable.start();
        }
    }

    public static class GifViewHolder extends RecyclerView.ViewHolder {
        protected SimpleDraweeView gif;
        protected Button viewFullSizeGIF;

        public GifViewHolder(View v) {
            super(v);
            gif = (SimpleDraweeView) v.findViewById(R.id.gifsdv);
            viewFullSizeGIF = (Button) v.findViewById(R.id.fullsizebutton);
        }
    }

    public void addData(ArrayList<GiphyResponse.GIF> data) {
        this.data.addAll(data);
        notifyItemRangeChanged(this.data.size() - data.size(), data.size());
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }
}