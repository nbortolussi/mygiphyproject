package com.mygiphyproject;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class FullSizeGIFActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_size_gif);

        setupActivity();
    }

    public void setupActivity() {
        GiphyResponse.GIF GIF = getIntent().getExtras().getParcelable("gif");
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.gifsdv);

        // let's get the screen width so we can properly calculate the height!
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float paddingPixels = getResources().getDimensionPixelSize(R.dimen.default_half_margin) * 2;
        int width = (int) (metrics.widthPixels - paddingPixels);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = GIF.getFullSizeHeight(width);
        simpleDraweeView.setLayoutParams(params);

        final MaterialProgressBar loadingprogress = (MaterialProgressBar) findViewById(R.id.loadingprogress);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(GIF.getFullSizeImageUri())
                .setAutoPlayAnimations(true)
                .setControllerListener(new ControllerListener<ImageInfo>() {
                    @Override
                    public void onSubmit(String id, Object callerContext) {

                    }

                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        loadingprogress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onIntermediateImageSet(String id, ImageInfo imageInfo) {

                    }

                    @Override
                    public void onIntermediateImageFailed(String id, Throwable throwable) {

                    }

                    @Override
                    public void onFailure(String id, Throwable throwable) {
                        loadingprogress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onRelease(String id) {

                    }
                })
                .build();
        simpleDraweeView.setController(controller);
    }
}
