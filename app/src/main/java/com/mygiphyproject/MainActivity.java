package com.mygiphyproject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.concurrent.Semaphore;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class MainActivity extends AppCompatActivity {

    private GetTrendingGifs getTrendingGifs;
    private SearchGifs searchGifs;
    private MaterialProgressBar loadingprogress;
    private GiphyAdapter giphyAdapter;
    private int offset = 0;
    private int limit = 25;
    private boolean searchEnabled;
    private EditText search;
    private String currentSearchTerm;
    private boolean hitEndOfList;
    private final Semaphore grabGifs = new Semaphore(1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActivity();
        getTrendingGifs();
    }

    public void setupActivity() {
        search = (EditText) findViewById(R.id.searchet);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView view, int arg1, KeyEvent arg2) {
                if ((arg1 == EditorInfo.IME_ACTION_SEARCH)) {
                    performSearch();
                }
                return false;
            }
        });
        loadingprogress = (MaterialProgressBar) findViewById(R.id.loadingprogress);

        // let's get the row width so we can properly calculate the height!
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float paddingPixels = getResources().getDimensionPixelSize(R.dimen.default_margin) * 3;
        int rowWidth = (int) ((metrics.widthPixels - paddingPixels) / 2);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.giphyrv);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        giphyAdapter = new GiphyAdapter(rowWidth);
        recyclerView.setAdapter(giphyAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!hitEndOfList && dy > 0) {
                    int[] firstItemPositions = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPositions(null);
                    for (int position : firstItemPositions) {
                        if (position == recyclerView.getAdapter().getItemCount() - 1) {
                            if (searchEnabled) {
                                searchGifs(currentSearchTerm);
                            } else {
                                getTrendingGifs();
                            }
                        }
                    }
                }
            }
        });
    }

    public void performSearch() {
        hitEndOfList = false;
        offset = 0;
        giphyAdapter.clearData();
        searchEnabled = true;
        currentSearchTerm = search.getText().toString();
        search.setVisibility(View.GONE);
        search.setText("");
        hideKeyboard();
        searchGifs(currentSearchTerm);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getTrendingGifs != null)
            getTrendingGifs.cancel(true);
        if (searchGifs != null)
            searchGifs.cancel(true);
    }

    public void getTrendingGifs() {
        if (grabGifs.tryAcquire()) {
            getTrendingGifs = new GetTrendingGifs();
            getTrendingGifs.execute();
        }
    }

    public class GetTrendingGifs extends AsyncTask<Void, Void, Boolean> {

        private GiphyResponse giphyResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingprogress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voided) {
            try {
                giphyResponse = RestClient.getGiphyInterface().getTrendingGifs(BuildConfig.api_key, offset, limit).execute().body();
                if (giphyResponse.getData().size() < limit)
                    hitEndOfList = true;
                offset = offset + 25;
                return true;
            } catch (IOException e) {
                Log.e(getClass().getName(), "Oops, our call to get GIFS failed.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            loadingprogress.setVisibility(View.GONE);
            addGiphyResponseToAdapter(giphyResponse);
            grabGifs.release();
        }
    }

    public void searchGifs(String query) {
        if (grabGifs.tryAcquire()) {
            searchGifs = new SearchGifs();
            searchGifs.execute(query);
        }
    }

    public class SearchGifs extends AsyncTask<String, Void, Boolean> {

        private GiphyResponse giphyResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingprogress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... query) {
            try {
                giphyResponse = RestClient.getGiphyInterface().searchGifs(BuildConfig.api_key, query[0], offset, limit).execute().body();
                if (giphyResponse.getData().size() < limit)
                    hitEndOfList = true;
                offset = offset + 25;
                return true;
            } catch (IOException e) {
                Log.e(getClass().getName(), "Oops, our call to get GIFS failed.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            loadingprogress.setVisibility(View.GONE);
            addGiphyResponseToAdapter(giphyResponse);
            grabGifs.release();
        }
    }

    public void addGiphyResponseToAdapter(GiphyResponse giphyResponse) {
        if (giphyResponse != null)
            giphyAdapter.addData(giphyResponse.getData());

        if (giphyAdapter.getItemCount() == 0)
            Toast.makeText(this, getString(R.string.fail), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_search:
                if (search.getVisibility() == View.GONE)
                    search.setVisibility(View.VISIBLE);
                else
                    search.setVisibility(View.GONE);
                break;
        }
        return true;
    }

    public void viewGIFClicked(View v) {
        Intent intent = new Intent(this, FullSizeGIFActivity.class);
        intent.putExtra("gif", (Parcelable) v.getTag());
        startActivity(intent);
    }
}
