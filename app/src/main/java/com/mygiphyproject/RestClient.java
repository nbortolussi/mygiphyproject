package com.mygiphyproject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nbortolussi on 5/20/16.
 */
public class RestClient {

    private static GiphyInterface giphyInterface;

    public static GiphyInterface getGiphyInterface() {
        if (giphyInterface == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.giphy.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            giphyInterface = retrofit.create(GiphyInterface.class);
        }

        return giphyInterface;
    }

    public interface GiphyInterface {

        @GET("v1/gifs/search")
        Call<GiphyResponse> searchGifs(@Query("api_key") String api_key, @Query("q") String q, @Query("offset") int offset, @Query("limit") int limit);

        @GET("v1/gifs/trending")
        Call<GiphyResponse> getTrendingGifs(@Query("api_key") String api_key, @Query("offset") int offset, @Query("limit") int limit);
    }
}
